rootProject.name = "jettpack"
include("jettpack-api", "jettpack-server")

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://papermc.io/repo/repository/maven-public/")
        maven("https://jitpack.io/")
        mavenCentral()
    }
}